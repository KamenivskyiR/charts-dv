const ctx = document.getElementById('myChart');

function createConfig(gridlines, title) {
  return {
    type: 'line',
    data: {
      labels: ['', '', '', '', '', '', '', '', '', ''],
      datasets: [
        {
          label: 'My First dataset',
          backgroundColor: 'rgb(12,154,72)',
          borderColor: 'rgb(12,154,72)',
          data: [22, 27, 68, 106, 92, 97, 92, 79, 79, 120],
          fill: false,
          pointRadius: 0,
          lineTension: 0,
          spanGaps: true,
        },
      ],
    },
    options: {
      responsive: true,
      title: {
        display: true,
        text: title,
      },
      scales: {
        xAxes: [
          {
            gridLines: gridlines,
          },
        ],
        yAxes: [
          {
            gridLines: gridlines,
            ticks: {
              min: 0,
              max: 150,
              stepSize: 10,
            },
          },
        ],
      },
    },
  };
}

window.onload = function () {
  const container = document.querySelector('.container');

  [
    {
      title: 'Display: true',
      gridLines: {
        display: true,
      },
    },
    // {
    //   title: 'Display: false',
    //   gridLines: {
    //     display: false,
    //   },
    // },
    // {
    //   title: 'Display: false, no border',
    //   gridLines: {
    //     display: false,
    //     drawBorder: false,
    //   },
    // },
    // {
    //   title: 'DrawOnChartArea: false',
    //   gridLines: {
    //     display: true,
    //     drawBorder: true,
    //     drawOnChartArea: false,
    //   },
    // },
    // {
    //   title: 'DrawTicks: false',
    //   gridLines: {
    //     display: true,
    //     drawBorder: true,
    //     drawOnChartArea: true,
    //     drawTicks: false,
    //   },
    // },
  ].forEach(function (details) {
    const div = document.createElement('div');
    div.classList.add('chart-container');

    const canvas = document.createElement('canvas');
    div.appendChild(canvas);
    container.appendChild(div);

    const ctx = canvas.getContext('2d');
    const config = createConfig(details.gridLines, details.title);
    new Chart(ctx, config);
  });
};
